__English__ | [Russian](README_ru.md)

### Description
This is a starter kit for running sibcoin masternode on top of Docker. 

### Requirements
*  [Docker](https://www.docker.com/community-edition#/download)
*  [Docker Compose](https://docs.docker.com/compose/install/#alternative-install-options)

### Local wallet configuration
Send to yourself 1000 SIB in one transaction, then type `masternode outputs` in wallet console (Tools -> Debug console). You will see a json answer in the following structure:

```json
{
	"93c9595ee5d8a8dc083299049282167345d701b35e282aa3ef4300418a513782": 1,
	"3ec2ac47709ac0dfa5c5a75ca63513dcba0aafb274b634004f94fc5d87f0f552": 0,
	"your_1000sib_transaction_id": "transaction_output"
}
```

Choose one pair which isn't linked with masternode yet. Also, generate masternode private key by command `masternode genkey`. Then open masternode.conf (Tools -> Open Masternode Configuration file) and add the following line:

```
mn1 123.123.123.123:1945 masternode_private_key your_1000sib_transaction_id transaction_output
```

* `mn1` is a masternode name (you can name it as you want)
* `123.123.123.123` is your masternode IPv4. Change `1945` to `11945` if you want to run over the test network (also you need to run wallet with -testnet flag).
* `masternode_private_key`, `your_1000sib_transaction_id`, `transaction_output` you get above in wallet console.

Save it and restart your local wallet.

### Masternode configuration
Create the following folders and files:
*  sibcoin (directory, you can name it as you want)
   *  docker-compose.yml (file)
   *  wallet (directory)
      *  sibcoin.conf (file)
      
Instead of creating you can simply clone this repo:
```bash
git clone https://gitlab.com/d3fl4t3/sibcoin-masternode-kit
```
<br>

`./docker-compose.yml` is a config for docker-compose. Create it in your favourite text editor and paste the following text:
```yaml
version: '2'
services:
  sibcoind:
    image: registry.gitlab.com/d3fl4t3/sibcoind-docker
    volumes:
     - ./wallet:/wallet  # ./wallet is a sibcoin datadir with sibcoin.conf, wallet.dat, etc.
    ports:
     - 123.123.123.123:1945:1945  # Change 123.123.123.123 to your external ip. Change 1945 to 11945 if running over the test network
    restart: always
  sentinel:
    image: registry.gitlab.com/d3fl4t3/sentinel
    volumes:
     - ./wallet/sibcoin.conf:/sibcoin.conf
    environment:
     - RPC_HOST=sibcoind
    restart: always
```
<br>
`./wallet/sibcoin.conf` is a sibcoin daemon config, simply create it with your favourite editor as you do it early and paste this text:
```ini
masternode=1
masternodeprivkey=CHANGEME

externalip=123.123.123.123
bind=0.0.0.0

server=1
rpcuser=user
rpcpassword=password
rpcport=4848
rpcallowip=0.0.0.0/0
```

*  Add `testnet=1` to sibcoin.conf if you want to run over the test network, also change `1945` to `11945` in docker-compose.yml
*  Replace `123.123.123.123` with your public IPv4 in both files. 
*  `CHANGEME` is a placeholder for your private key generated by command `masternode genkey` in your local wallet's console. 
*  You can change parameters `rpcuser`, `rpcpassword`, `rpcport` but it's not really important because JSON-RPC interface is available only in local docker environment.

### Run
Type `docker-compose up -d` in your command line. That's all! Wait 30-40 minutes while blockchain syncing, then start masternode from your local wallet console: `masternode start-all`. Within 3-5 minutes masternode status will turn to PRE_ENABLED, then to ENABLED.