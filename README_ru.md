[English](README.md) | __Russian__

### Описание
Этот репозиторий представляет собой готовый набор конфигов для запуска мастерноды Сибирского Червонца через Docker.

### Необходимый софт
*  [Docker](https://www.docker.com/community-edition#/download)
*  [Docker Compose](https://docs.docker.com/compose/install/#alternative-install-options)

### Настройка локального кошелька
Отправьте самому себе 1000 Ч одной транзакцией, затем напишите `masternode outputs` в консоли кошелька (Инструменты -> Консоль отладки). Результат выполнения будет выглядеть примерно так:

```json
{
	"93c9595ee5d8a8dc083299049282167345d701b35e282aa3ef4300418a513782": 1,
	"3ec2ac47709ac0dfa5c5a75ca63513dcba0aafb274b634004f94fc5d87f0f552": 0,
	"your_1000sib_transaction_id": "transaction_output"
}
```

Выберите ту транзакцию, которую вы ещё не использовали для активации мастерноды. Также сгенерируйте приватный ключ мастерноды командой `masternode genkey`. Наконец откройте masternode.conf (Инструменты -> Открыть файл настроек мастернод) и добавьте следующую строку:

```
mn1 123.123.123.123:1945 masternode_private_key your_1000sib_transaction_id transaction_output
```

* `mn1` это название мастерноды (можете вводить любое)
* `123.123.123.123` это IP вашей мастерноды. Измените `1945` на `11945` если хотите запустить мастерноду в тестовой сети (также вам нужно запускать кошелёк с параметром -testnet).
* `masternode_private_key`, `your_1000sib_transaction_id`, `transaction_output` это те значения, которые вы получили в консоли ранее (приватный ключ мастерноды, id и output транзакции соответственно).

Сохраните файл и перезапустите ваш локальный кошелёк.

### Настройка мастерноды
Создайте следующие папки и файлы:
*  sibcoin (папка, имя любое)
   *  docker-compose.yml (файл)
   *  wallet (папка)
      *  sibcoin.conf (файл)
      
Вместо того чтобы делать это руками, вы можете просто скачать этот репозиторий:
```bash
git clone https://gitlab.com/d3fl4t3/sibcoin-masternode-kit
```
<br>

`./docker-compose.yml` это конфиг для docker-compose. Создайте его своим любимым текстовым редактором и впишите следующий текст:
```yaml
version: '2'
services:
  sibcoind:
    image: registry.gitlab.com/d3fl4t3/sibcoind-docker
    volumes:
     - ./wallet:/wallet  # ./wallet это папка с данными сервера: тут будут лежать sibcoin.conf, wallet.dat и т.д.
    ports:
     - 123.123.123.123:1945:1945  # Измените 123.123.123.123 на ваш внешний IP. Измените 1945 на 11945 если запускаете мастерноду в тестовой сети.
    restart: always
  sentinel:
    image: registry.gitlab.com/d3fl4t3/sentinel
    volumes:
     - ./wallet/sibcoin.conf:/sibcoin.conf
    environment:
     - RPC_HOST=sibcoind
    restart: always
```
<br>
`./wallet/sibcoin.conf` это конфиг ноды Сибирского Червонца, как и ранее, создайте его своим любимым текстовым редактором и впишите следующее:
```ini
masternode=1
masternodeprivkey=CHANGEME

externalip=123.123.123.123
bind=0.0.0.0

server=1
rpcuser=user
rpcpassword=password
rpcport=4848
rpcallowip=0.0.0.0/0
```

*  Добавьте `testnet=1` в sibcoin.conf если хотите запустить ноду в тестовой сети, также измените `1945` на `11945` в docker-compose.yml
*  Замените `123.123.123.123` на ваш внешний IP в обоих файлах. 
*  Замените `CHANGEME` на ваш приватный ключ, созданный командой `masternode genkey` в вашем локальном кошельке ранее. 
*  Вы можете изменить `rpcuser`, `rpcpassword`, `rpcport`, но это не имеет большого значения, так как интерфейс RPC доступен только во внутренней сети докера.

### Запуск
Введите `docker-compose up -d` в вашем терминале/командной строке. Всё готово! Подождите 30-40 минут до полной синхронизации блокчейна, затем запустите мастерноду из вашего локального кошелька: `masternode start-all`. Через 3-5 минут статус мастерноды станет PRE_ENABLED, затем ENABLED.